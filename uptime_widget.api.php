<?php

/**
 * @file
 * Hooks provided by uptime_widget module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Modify the list of available widgets, provided by this module by default.
 *
 * To alter available options, set something like:
 * $options['example'] = t('Example').
 *
 * @param array $options
 *   An array with list of widgets, provided by default.
 */
function hook_uptime_widget_type_alter(array &$options) {
}
