CONTENTS OF THIS FILE
------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
    * Getting an API key and a monitor ID from the UptimeRobot service
    * Using the module's settings
    * Configuring the copyright notice
    * Placing and configuring the uptime widget block
 * FAQ
 * Troubleshooting
 * Maintainers

 INTRODUCTION
------------------

The UpTime Widget module connects your website to a free uptime monitoring
service — UpTimeRobot.com. It shows the percentage of time that your website
is available to visitors online. This widget can be placed anywhere on your
website as a Drupal block. It can also optionally show a configurable copyright
notice.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/uptime_widget
 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/uptime_widget

REQUIREMENTS
------------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
------------------

Getting an API key and a monitor ID from the UptimeRobot service
------------------

Register your website on  the UptimeRobot.com service and get the API key
and the monitor ID. Here are the steps:

* sign up, activate your account, and log in to UptimeRobot.com
* add a new monitor of the HTTP(s) type, give our website a name, and submit
its URL
* go to "My Settings", create the main API key (in the right bottom corner),
and copy it to clipboard
* go to http://api.uptimerobot.com/getMonitors?apiKey=FILL-IN-YOUR-API-KEY-HERE
and the API key instead of the last part of the URL
* refresh the page and see the monitor ID generated (monitor id = "a few digits
here").
* save the API key and monitor ID and go to your Drupal website.

The service has a dashboard and notification settings that you can visit at any
 time.

Configuring the module
------------------

When the module is installed and enabled, its settings appear at
admin/config/system/uptime_widget. There are two key required fields where you
need to enter the previously received API key and monitor ID.

The "decimal separator" and "scale" fields have good defaults, but you can play
with the ways our website uptime digits are displayed.

The monitoring interval and the refresh interval fields also have sensible
defaults. But you can choose how often the website uptime should be checked and
 how often Drupal should receive this information.

Configuring the copyright notice
------------------

The website uptime widget by default comes with the copyright widget, which can
 optionally be disabled. Hiding or showing the copyright is also available in
 the block configuration, which will be described in the "Placing and
 configuring the uptime widget block" part.

On the module's settings page at admin/config/system/uptime_widget, you can
configure how the copyright will look. It offers:

* several options for the copyright notice
* the option to specify the year that our domain was first online
* the option to write a custom "Prepend text" instead of "All rights reserved."

Placing and configuring the uptime widget block
------------------

In Structure — Block Layout, choose the theme region (for example, Footer
first), click on it, find the Uptime block in the list of blocks, place block,
and save the blocks.

You can configure the block either on the Block Layout page or by clicking the
"quick edit" pencil near the block on the website.

It's possible to leave or hide the block title by checking or unchecking
"Display title," configure visibility for specific roles, specific pages or
content types, and so on.

You can also choose to show both the uptime and copyright widgets, or only one
of them.

FAQ
------------------

Q: "Can I reset the monitor?"
A: Go to http://www.uptimerobot.com/myMonitors.asp. In the 'Actions' column
delete the monitor and re-create it again.

Q: "During development my site might be down. Can I pause the monitor?"
A: Disable uptime in the settings form. This pauses monitoring and removes
the ratio display.

TROUBLESHOOTING
------------------

The UpTime gets refreshed every 24 hours or after saving the UpTime settings.
If the region 'footer' is not available in the used theme, the block should be
enabled "manually".

MAINTAINERS
------------------

Current maintainers:
 * Volodymyr Knyshuk (knyshuk.vova) - https://www.drupal.org/u/knyshukvova

Current co-maintainers:
 * Martin Postma (lolandese) - https://www.drupal.org/u/lolandese
 * Volodymyr Reshetnikov (Vladimirrem) - https://www.drupal.org/u/vladimirrem
 * Oleg Kuzava (ApacheEx) - https://www.drupal.org/u/apacheex
